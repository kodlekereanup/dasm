# TODO

A basic abstracted NLP pipline of the project

- Dataset Collection (sentiment140 + ... for training and testing)
- Feature Selection  (tf-idf?)
- Feature Extraction (tf-idf?) 
- Data Preprocessing (lemmatization, tokenization, stop words, pos, stemming etc)
- Data Representation (vectorization, word embeddings, tf-idf, ngrams, bagofwords)
- Analysis (RNN + LSTM)
- Visualizations

# NOTES

Feature selection and extraction methods. Also we need a metric to measure which feature is the best.
So basically every word will have a weight associated with it. The higher the weight the more valuable it is.

# Foreseeable Problems

- How to handle sarcasm, irony etc
- Should twitter handle be considered? (@Amazon could give the necessary context needed to understand who is the person
  talking about). But this will open gates to other nonsensical twitter handles which would just be noise.

# TOOLS

Maybe use AWS / Google Cloud for training RNN + LSTM.

### Deep learning frameworks:

- Tensorflow
- Keras
- Caffe
- Theano
- PyTorch
- fast.ai
- FastText
- autograd
- micrograd
- tinygrad

### Data Science tools:

- NumPy
- SciPy
- Scikit-Learn
- Matplotlib

### NLP

- Gensim
- Word2Vec
- NLTK
- CoreNLP
- SpaCy
- TextBlob

# Advanced

- BERT
- Transformers
- (Auto)encoders
