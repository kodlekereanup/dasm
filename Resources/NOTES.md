# Sequence Models

## Anchoring
People's opinion change over time. For example, ratings of a movie can increase by half a point after it
wins an oscar, even though it is the same movie. This could obviously be directed at the number of people
watching the movie as it will definitely increase after a prestigious award grab, but it isn't wrong to 
assume that the change is linear. Whatever is the increase in the number of people watching the movie, 
there will certainly be people who will rate it negatively as well.

## Hedonic Adaptation

There is the hedonic adaptation, where humans quickly adapt to accept an improved or a worsened situation 
as the new normal. For instance, after watching many good movies, the expectations that the next movie 
is equally good or better are high. Hence, even an average movie might be considered as bad after many 
great ones are watched.

## Seasonality
Christmas themed (or more generally, festive-themed) movies which are aired only during the respective
event.

## External Factors
In a lot of cases, movies have lost rating because of the people directly related to it in controversy.
Cancel culture being the most relevant example.

Language and Speech are sequential. Only when you put those words in a particular order (also respecting
language grammer) does the sentence make sense. This is where other NNs fail and the combination of
RNN and LSTM shines. 
