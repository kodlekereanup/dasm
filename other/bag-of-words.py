# load dataset from csv file and convert into list

'''
dataset = ['Hello, how are you!',
             'Win money, win from home.',
             'Call me now.',
             'Hello, Call hello you tomorrow?']
'''

import pandas as pd
import numpy as np

cols = ['sentiment','id','date','query_string','user','text']
df = pd.read_csv('datasets/training.1600000.processed.noemoticon.csv', engine = "python", header=None, names=cols)

# has the tweets from the Sentiment140 dataset converted into 
# a list
tweets = (df.iloc[0:1000, -1:])['text'].to_list()

for i in range(5):
    print("{}: {}\n".format(i, tweets[i]))


# pre-process the dataset
# convert to lowercase, remove punctuations if any
# and tokenize the input stream

pre_processed_dataset = []

for i in tweets:
    i = i.lower()
    i = (''.join(c for c in i if c not in  "!\"#$%&'()*+,-./:;<=>?@[\]^_`{|}~"))
    pre_processed_dataset.append(i.split(' '))
    
print(len(pre_processed_dataset))

# bag of words algorithm:
# calculate the frequency of each word in every sentence

frequency_list = []
import pprint as pp
from collections import Counter

for i in pre_processed_dataset:
    frequency_list.append(Counter(i))

pp.pprint(frequency_list)
