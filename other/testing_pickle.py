import nltk_review_3 as source
import pickle
import sys

if len(sys.argv) != 2:
    print('\n[Argument Error]: Tweet not passed as argument. Check usage \n')
    print('Usage: python filename "tweet"')
    sys.exit()

# unpickling the classifier model
classifier = pickle.load(open('model.pickle', 'rb'))

# loading the tweet passed from the command line (or API)
tweet = sys.argv[1] 

print(classifier.classify(dict([token, True] for token in source.remove_noise(source.word_tokenize(tweet)))))
